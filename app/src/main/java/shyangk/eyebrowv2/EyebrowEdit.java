package shyangk.eyebrowv2;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;

import library.MoveGestureDetector;
import library.RotateGestureDetector;
import library.ShoveGestureDetector;

public class EyebrowEdit extends Activity implements View.OnClickListener, OnTouchListener {

    //Touch Variables

    private Matrix mMatrix = new Matrix();
    private float mScaleFactor = .4f;
    private float mRotationDegrees = 0.f;
    private float mFocusX = 0.f;
    private float mFocusY = 0.f;
    private int mAlpha = 255;
    private int mImageHeight, mImageWidth;
    private ScaleGestureDetector mScaleDetector;
    private RotateGestureDetector mRotateDetector;
    private MoveGestureDetector mMoveDetector;
    private ShoveGestureDetector mShoveDetector;

    LinearLayout asthmaActionPlan, controlledMedication, asNeededMedication,
            rescueMedication, yourSymtoms, yourTriggers, wheezeRate, peakFlow;
    LayoutParams params;
    LinearLayout next, prev;
    int viewWidth;
    GestureDetector gestureDetector = null;
    HorizontalScrollView horizontalScrollView;
    ArrayList<LinearLayout> layouts;
    int parentLeft, parentRight;
    int mWidth;
    int currPosition, prevPosition;
    ImageView image0, image1, image2, image3, image4, image5, image6, image7;
    ImageView view;
    Uri myUri;

    private Uri mSavedFileUri;
    public Button btnSave;
    private Button btnShare;

    private FrameLayout cameraFrame;
    SaveImage saveImage = new SaveImage();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eyebrow_edit);



        // Determine the center of the screen to center 'earth'
        Display display = getWindowManager().getDefaultDisplay();
        mFocusX = display.getWidth()/2f;
        mFocusY = display.getHeight()/2f;

        // Set this class as touchListener to the ImageView
        view = (ImageView) findViewById(R.id.imageView);
        view.setOnTouchListener(this);

        // Determine dimensions of 'earth' image
        Drawable d 		= this.getResources().getDrawable(R.drawable.eyebrow);
        mImageHeight 	= d.getIntrinsicHeight();
        mImageWidth 	= d.getIntrinsicWidth();

        // View is scaled and translated by matrix, so scale and translate initially
        float scaledImageCenterX = (mImageWidth*mScaleFactor)/2;
        float scaledImageCenterY = (mImageHeight*mScaleFactor)/2;

        mMatrix.postScale(mScaleFactor, mScaleFactor);
        mMatrix.postTranslate(mFocusX - scaledImageCenterX, mFocusY - scaledImageCenterY);
        view.setImageMatrix(mMatrix);
        view.setOnTouchListener(this);

        // Setup Gesture Detectors
        mScaleDetector 	= new ScaleGestureDetector(getApplicationContext(), new ScaleListener());
        mRotateDetector = new RotateGestureDetector(getApplicationContext(), new RotateListener());
        mMoveDetector 	= new MoveGestureDetector(getApplicationContext(), new MoveListener());
        mShoveDetector 	= new ShoveGestureDetector(getApplicationContext(), new ShoveListener());

        cameraFrame = (FrameLayout) findViewById(R.id.cameraImage);
        prev = (LinearLayout) findViewById(R.id.prev);
        next = (LinearLayout) findViewById(R.id.next);
        horizontalScrollView = (HorizontalScrollView) findViewById(R.id.hsv);
        gestureDetector = new GestureDetector(new MyGestureDetector());
        asthmaActionPlan = (LinearLayout) findViewById(R.id.asthma_action_plan);
        controlledMedication = (LinearLayout) findViewById(R.id.controlled_medication);
        asNeededMedication = (LinearLayout) findViewById(R.id.as_needed_medication);
        rescueMedication = (LinearLayout) findViewById(R.id.rescue_medication);
        yourSymtoms = (LinearLayout) findViewById(R.id.your_symptoms);
        yourTriggers = (LinearLayout) findViewById(R.id.your_triggers);
        wheezeRate = (LinearLayout) findViewById(R.id.wheeze_rate);
        peakFlow = (LinearLayout) findViewById(R.id.peak_flow);
        display = getWindowManager().getDefaultDisplay();
        mWidth = display.getWidth(); // deprecated
        viewWidth = mWidth / 3;
        layouts = new ArrayList<LinearLayout>();
        params = new LayoutParams(viewWidth, LayoutParams.WRAP_CONTENT);
        asthmaActionPlan.setLayoutParams(params);
        controlledMedication.setLayoutParams(params);
        asNeededMedication.setLayoutParams(params);
        rescueMedication.setLayoutParams(params);
        yourSymtoms.setLayoutParams(params);
        yourTriggers.setLayoutParams(params);
        wheezeRate.setLayoutParams(params);
        peakFlow.setLayoutParams(params);
        layouts.add(asthmaActionPlan);
        layouts.add(controlledMedication);
        layouts.add(asNeededMedication);
        layouts.add(rescueMedication);
        layouts.add(yourSymtoms);
        layouts.add(yourTriggers);
        layouts.add(wheezeRate);
        layouts.add(peakFlow);

        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);
        btnSave.setId(8);

        btnShare = (Button) findViewById(R.id.btnShare);
        btnShare.setOnClickListener(this);

        image0 = (ImageView) findViewById(R.id.image0);
        image0.setOnClickListener(this);
        image0.setId(0);

        image1 = (ImageView) findViewById(R.id.image1);
        image1.setOnClickListener(this);
        image1.setId(1);

        image2 = (ImageView) findViewById(R.id.image2);
        image2.setOnClickListener(this);
        image2.setId(2);

        image3 = (ImageView) findViewById(R.id.image3);
        image3.setOnClickListener(this);
        image3.setId(3);

        image4 = (ImageView) findViewById(R.id.image4);
        image4.setOnClickListener(this);
        image4.setId(4);

        image5 = (ImageView) findViewById(R.id.image5);
        image5.setOnClickListener(this);
        image5.setId(5);

        image6 = (ImageView) findViewById(R.id.image6);
        image6.setOnClickListener(this);
        image6.setId(6);

        image7 = (ImageView) findViewById(R.id.image7);
        image7.setOnClickListener(this);
        image7.setId(7);

        try {
            Bundle extras = getIntent().getExtras(); // get Bundle of extras
            myUri = Uri.parse(extras.getString("imageUri"));
            System.out.println(myUri);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            InputStream is = this.getContentResolver().openInputStream(myUri);
            BitmapFactory.Options options=new BitmapFactory.Options();
            options.inSampleSize = 1;
            Bitmap bitmap= BitmapFactory.decodeStream(is, null, options);
            Drawable background = new BitmapDrawable(getResources(),bitmap);
            cameraFrame.setBackground(background);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
    class MyGestureDetector extends SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                               float velocityY) {
            if (e1.getX() < e2.getX()) {
                currPosition = getVisibleViews("left");
            } else {
                currPosition = getVisibleViews("right");
            }

            horizontalScrollView.smoothScrollTo(layouts.get(currPosition)
                    .getLeft(), 0);
            return true;
        }
    }
    public int getVisibleViews(String direction) {
        Rect hitRect = new Rect();
        int position = 0;
        int rightCounter = 0;
        for (int i = 0; i < layouts.size(); i++) {
            if (layouts.get(i).getLocalVisibleRect(hitRect)) {
                if (direction.equals("left")) {
                    position = i;
                    break;
                } else if (direction.equals("right")) {
                    rightCounter++;
                    position = i;
                    if (rightCounter == 2)
                        break;
                }
            }
        }
        return position;
    }

    //Touch Methods ******************************************


    @SuppressWarnings("deprecation")
    public boolean onTouch(View v, MotionEvent event) {
        mScaleDetector.onTouchEvent(event);
        mRotateDetector.onTouchEvent(event);
        mMoveDetector.onTouchEvent(event);
//        mShoveDetector.onTouchEvent(event);

        float scaledImageCenterX = (mImageWidth*mScaleFactor)/2;
        float scaledImageCenterY = (mImageHeight*mScaleFactor)/2;

        mMatrix.reset();
        mMatrix.postScale(mScaleFactor, mScaleFactor);
        mMatrix.postRotate(mRotationDegrees,  scaledImageCenterX, scaledImageCenterY);
        mMatrix.postTranslate(mFocusX - scaledImageCenterX, mFocusY - scaledImageCenterY);

        ImageView view = (ImageView) v;
        view.setImageMatrix(mMatrix);
        view.setAlpha(mAlpha);

        return true; // indicate event was handled
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            mScaleFactor *= detector.getScaleFactor(); // scale change since previous event

            // Don't let the object get too small or too large.
            mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 10.0f));

            return true;
        }
    }

    private class RotateListener extends RotateGestureDetector.SimpleOnRotateGestureListener {
        @Override
        public boolean onRotate(RotateGestureDetector detector) {
            mRotationDegrees -= detector.getRotationDegreesDelta();
            return true;
        }
    }

    private class MoveListener extends MoveGestureDetector.SimpleOnMoveGestureListener {
        @Override
        public boolean onMove(MoveGestureDetector detector) {
            PointF d = detector.getFocusDelta();
            mFocusX += d.x;
            mFocusY += d.y;

            // mFocusX = detector.getFocusX();
            // mFocusY = detector.getFocusY();
            return true;
        }
    }

    private class ShoveListener extends ShoveGestureDetector.SimpleOnShoveGestureListener {
        @Override
        public boolean onShove(ShoveGestureDetector detector) {
            mAlpha += detector.getShovePixelsDelta();
            if (mAlpha > 255)
                mAlpha = 255;
            else if (mAlpha < 0)
                mAlpha = 0;

            return true;
        }
    }

    public void addToPreviewFrame(View view) {

        cameraFrame.addView(view);
        int children = cameraFrame.getChildCount();
    }

    //TODO: Return success or failure
    public void removeFromPreviewFrame(View view) {
        int children = cameraFrame.getChildCount();
        cameraFrame.removeView(view);
    }

    public static Bitmap loadBitmapFromView(View v) {
        System.out.println("width: " + v.getHeight());
        Bitmap b = Bitmap.createBitmap( v.getWidth(), v.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);
        return b;
    }

    @Override
    public void onClick(View v) {
        int buttonId = v.getId();

        switch (buttonId) {
            case 0:
                view.setImageResource(R.drawable.eyebrow);
                break;
            case 1:
                view.setImageResource(R.drawable.eyebrow1);
                break;
            case 2:
                view.setImageResource(R.drawable.eyebrow);
                break;
            case 3:
                view.setImageResource(R.drawable.eyebrow1);
                break;
            case 4:
                view.setImageResource(R.drawable.eyebrow);
                break;
            case 5:
                view.setImageResource(R.drawable.eyebrow1);
                break;
            case 6:
                view.setImageResource(R.drawable.eyebrow);
                break;
            case 7:
                view.setImageResource(R.drawable.eyebrow1);
                break;
            case 8:
                saveImage.updateDrawingCache(cameraFrame);
                Toast.makeText(this, "Image Saved!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnShare:
                if (mSavedFileUri == null){
                    //Save the file first
                    mSavedFileUri = saveImage.updateDrawingCache(cameraFrame);
                }
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_STREAM, mSavedFileUri);
                shareIntent.setType("image/jpeg");
                startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.send_to)));

                  break;
        }

    }
}