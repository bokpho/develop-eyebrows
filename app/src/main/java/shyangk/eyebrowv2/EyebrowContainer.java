package shyangk.eyebrowv2;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class EyebrowContainer extends Activity implements EditPhoto.OnFragmentInteractionListener, View.OnClickListener{

    public final static String TAG = EyebrowContainer.class.getSimpleName();

    private Uri mContentUri;
    private Uri mSavedContentUri;

    private FrameLayout mPreviewFrame;

    private ImageButton actionForward;
    private ImageButton actionBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eyebrow_container);

        Intent intent = getIntent();
        mContentUri = intent.getData();

        actionForward = (ImageButton) findViewById(R.id.action_forward);
        actionForward.setOnClickListener(this);

        Fragment editPhotoFragment = new EditPhoto();
        Bundle args = new Bundle();

        String mContentUriStr = getFromMediaUri(getContentResolver(), mContentUri);
        args.putString("Uri", mContentUriStr);
        editPhotoFragment.setArguments(args);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.fragment_action_container, editPhotoFragment)
                            .commit();
        }

        mPreviewFrame = (FrameLayout) findViewById(R.id.fullscreen_content);
        displayImage(mContentUri);


    }

    public String getFromMediaUri(ContentResolver resolver, Uri uri) {
        if (uri == null) return null;

        if ("file".equals(uri.getScheme())) {
            return uri.getPath();
        } else if ("content".equals(uri.getScheme())) {
            final String[] filePathColumn = { MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.DISPLAY_NAME };
            Cursor cursor = null;
            try {
                cursor = resolver.query(uri, filePathColumn, null, null, null);
                if (cursor != null && cursor.moveToFirst()) {
                    final int columnIndex = (uri.toString().startsWith("content://com.google.android.gallery3d")) ?
                            cursor.getColumnIndex(MediaStore.MediaColumns.DISPLAY_NAME) :
                            cursor.getColumnIndex(MediaStore.MediaColumns.DATA);
                    // Picasa image on newer devices with Honeycomb and up
                    if (columnIndex != -1) {
                        String filePath = cursor.getString(columnIndex);
                        if (!TextUtils.isEmpty(filePath)) {
                            return filePath;
                        }
                    }
                }
            } catch (SecurityException ignored) {
                // Nothing we can do
            } finally {
                if (cursor != null) cursor.close();
            }
        }
        return null;
    }

    private void displayImage(Uri imageUri) {

        Matrix matrix = new Matrix();
        Bitmap mbitmap;
        try {
            mbitmap = MediaStore.Images.Media.getBitmap(
                    getContentResolver(), imageUri);
            mbitmap = Bitmap.createBitmap(mbitmap, 0, 0, mbitmap.getWidth(), mbitmap.getHeight(), matrix, false);
            ImageView myImageView = new ImageView(this);
            myImageView.setImageBitmap(mbitmap);

            mPreviewFrame.addView(myImageView);

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    @Override
    public void addToPreviewFrame(View view) {

        mPreviewFrame.addView(view);
        int children = mPreviewFrame.getChildCount();

        Log.i(TAG, "There are children " + children);
    }


    //TODO: Return success or failure
    @Override
    public void removeFromPreviewFrame(View view) {
        int children = mPreviewFrame.getChildCount();

        Log.i(TAG, "There are children " + children);
        mPreviewFrame.removeView(view);
    }


    @Override
    public void resetCanvas() {
        int children = mPreviewFrame.getChildCount();

        Log.i(TAG, "There are children " + children);

        if (children > 1) {
            mPreviewFrame.removeViewAt(children - 1);
        }

    }

    @Override
    public boolean drawViewSet(View view) {

        for (int i = 0; i < mPreviewFrame.getChildCount(); i++) {
            if (mPreviewFrame.getChildAt(i) == view) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.action_forward:
                Uri outputUri = Uri.fromFile(new File(getCacheDir(), "edited"));

                SaveImage.saveLayoutCache(mPreviewFrame, outputUri);

                System.out.println(outputUri);

                Intent eyeBrowEdit = new Intent(this, EyebrowEdit.class);
                Bundle extras = new Bundle();
                extras.putString("imageUri", outputUri.toString());
                eyeBrowEdit.putExtras(extras);
                startActivity(eyeBrowEdit);

                break;
        }
    }
}
