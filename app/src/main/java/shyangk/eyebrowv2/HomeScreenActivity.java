package shyangk.eyebrowv2;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.soundcloud.android.crop.Crop;

import java.io.File;


public class HomeScreenActivity extends Activity {

    public static final int REQUEST_SELECT = 1;

    private ImageButton cameraButton;
    private ImageButton galleryButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home_screen);

        cameraButton = (ImageButton) findViewById(R.id.iconCamera);
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeScreenActivity.this, EyebrowMain.class);
                startActivity(intent);
            }
        });

        galleryButton = (ImageButton) findViewById(R.id.iconGallery);
        galleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_GET_CONTENT).setType("image/*");
                startActivityForResult(intent, REQUEST_SELECT);

            }
        });



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == 1) {
            Uri imageUri = data.getData();
            if (imageUri != null) {

                Uri outputUri = Uri.fromFile(new File(getCacheDir(), "cropped"));
                new Crop(imageUri).output(outputUri).asSquare().start(this);

            } else {
                Toast.makeText(this, "No image selected", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == Crop.REQUEST_CROP && resultCode == Activity.RESULT_OK) {
            Uri imageUri = Crop.getOutput(data);
            if (imageUri != null) {

                Intent intent = new Intent(this, EyebrowContainer.class);
                intent.setData(imageUri);
                startActivity(intent);

            }
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(data).getMessage(), Toast.LENGTH_SHORT).show();

        } else if (resultCode != RESULT_CANCELED) {
            Toast.makeText(this, "No image selected", Toast.LENGTH_LONG).show();
        }
    }

}
