package shyangk.eyebrowv2;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import android.content.Intent;
import android.provider.MediaStore;

/**
 * Created by bachvo on 15-04-12.
 */
public class SaveImage {

    private Uri mContentUri;
    private byte[] mCameraData;
    private static Bitmap background_image;
    private Bitmap eyeDropper;

    public static Uri updateDrawingCache(FrameLayout frameLayout){
        File pictureFile = getOutputMediaFile();
        frameLayout.buildDrawingCache();
        System.out.println(frameLayout);
        Bitmap b1 = frameLayout.getDrawingCache();
        // copy this bitmap otherwise distroying the cache will destroy
        // the bitmap for the referencing drawable and you'll not
        // get the captured view
        background_image = b1.copy(Bitmap.Config.ARGB_8888, false);
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            background_image.compress(Bitmap.CompressFormat.PNG, 100, fos); // bmp is your Bitmap instance
            fos.flush();
            fos.close();
        } catch (Exception e) {
        }
        frameLayout.destroyDrawingCache();
        System.out.println("saved");

        return Uri.fromFile(pictureFile);
    }

    public static void saveLayoutCache(FrameLayout frameLayout, Uri fileUri){
        File pictureFile = new File(fileUri.getPath());
        frameLayout.buildDrawingCache();
        System.out.println(frameLayout);
        Bitmap b1 = frameLayout.getDrawingCache();
        // copy this bitmap otherwise distroying the cache will destroy
        // the bitmap for the referencing drawable and you'll not
        // get the captured view
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            b1.compress(Bitmap.CompressFormat.PNG, 100, fos); // bmp is your Bitmap instance
            fos.flush();
            fos.close();
        } catch (Exception e) {
        }
        frameLayout.destroyDrawingCache();
        System.out.println("saved");
    }

    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "EyebrowApp");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("EyebrowApp", "failed to create directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }
}
