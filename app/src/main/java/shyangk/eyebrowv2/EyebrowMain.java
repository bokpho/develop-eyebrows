package shyangk.eyebrowv2;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.media.FaceDetector;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

//Touch Imports
import android.graphics.drawable.Drawable;
import android.view.Display;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View.OnTouchListener;

import com.soundcloud.android.crop.Crop;

import library.MoveGestureDetector;
import library.RotateGestureDetector;
import library.ShoveGestureDetector;


public class EyebrowMain extends Activity implements View.OnClickListener {


    public static final int ORIENTATION = 90;

    private FrameLayout mPreviewFrame;
    private RelativeLayout mCameraButtons;
    private LinearLayout mImageButtons;

    private ImageButton mCameraButton;
    private ImageButton mGalleryButton;
    private FrameLayout mAcceptButton;
    private FrameLayout mDiscardButton;
    private ImageButton mPopupMenu;

    private Camera mCamera;
    private CameraPreview mCameraPreview;

    private byte[] mCameraData;
    private Uri mContentUri;


    private static final String TAG = EyebrowMain.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eyebrow_main);

        getCameraSetPreview();
        mCameraButtons = (RelativeLayout) findViewById(R.id.button_holder_customized_cam);
        mImageButtons = (LinearLayout) findViewById(R.id.button_holder_ok_cancel);

        mCameraButton = (ImageButton) findViewById(R.id.shutter_customized_camera);
        mCameraButton.setOnClickListener(this);

        mGalleryButton = (ImageButton) findViewById(R.id.gallery_customized_camera);
        mGalleryButton.setOnClickListener(this);

        mAcceptButton = (FrameLayout) findViewById(R.id.accept_customized_camera);
        mAcceptButton.setOnClickListener(this);

        mDiscardButton = (FrameLayout) findViewById(R.id.discard_customized_camera);
        mDiscardButton.setOnClickListener(this);

        mPopupMenu = (ImageButton) findViewById(R.id.popup_customized_camera);
        mPopupMenu.setOnClickListener(this);

    }

    @Override
    public void onResume()
    {
        super.onResume();

        //Only restart the camera preview if there is no child in the frame layout
        if (mCamera == null && mPreviewFrame.getChildCount() == 0){
            getCameraSetPreview();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mCamera != null){
            releaseCameraAndPreview();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == 1) {
            Uri imageUri = data.getData();
            mContentUri = imageUri;
            if (imageUri != null) {

                Uri outputUri = Uri.fromFile(new File(getCacheDir(), "cropped"));
                new Crop(imageUri).output(outputUri).asSquare().start(this);

            } else {
                Toast.makeText(this, "No image selected", Toast.LENGTH_LONG).show();
                releaseCameraAndPreview();
                getCameraSetPreview();
            }
        } else if (requestCode == Crop.REQUEST_CROP && resultCode == Activity.RESULT_OK) {
            Uri imageUri = Crop.getOutput(data);
            if (imageUri != null) {

                Intent intent = new Intent(this, EyebrowContainer.class);
                intent.setData(imageUri);
                startActivity(intent);
                }
        } else if (resultCode == Crop.RESULT_ERROR) {
                Toast.makeText(this, Crop.getError(data).getMessage(), Toast.LENGTH_SHORT).show();

        } else if (resultCode != RESULT_CANCELED) {
            Toast.makeText(this, "No image selected", Toast.LENGTH_LONG).show();
        }
    }


    private void displayImage(Uri imageUri) {

        Matrix matrix = new Matrix();
        Bitmap mbitmap;
        try {
            mbitmap = MediaStore.Images.Media.getBitmap(
                    getContentResolver(), imageUri);
            mbitmap = Bitmap.createBitmap(mbitmap, 0, 0, mbitmap.getWidth(), mbitmap.getHeight(), matrix, false);
            ImageView myImageView = new ImageView(this);
            myImageView.setImageBitmap(mbitmap);

            mPreviewFrame.addView(myImageView);

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private int getCameraPhotoOrientation(Context context, Uri imageUri, String imagePath){
        int rotate = 0;
        try {
            context.getContentResolver().notifyChange(imageUri, null);
            File imageFile = new File(imagePath);

            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }

            Log.i("RotateImage", "Exif orientation: " + orientation);
            Log.i("RotateImage", "Rotate value: " + rotate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }



    /**
     * Helper method to access the camera returns null if it cannot get the
     * camera or does not exist
     *
     * @return
     */
    private Camera getCameraInstance() {
        Camera camera = null;
        try {
            camera = Camera.open();
        } catch (Exception e) {
            // cannot get camera or does not exist
        }
        return camera;
    }

    // Helper method to get Camera Instance and start the preview
    private void getCameraSetPreview() {
        mCamera = getCameraInstance();
        if (mCamera != null) {
            Camera.Parameters parameters = mCamera.getParameters();
            mCamera.setDisplayOrientation(ORIENTATION);
            parameters.setRotation(ORIENTATION); //set rotation to save the picture

            // TODO Set proper resolution for camera
            parameters.setPictureSize(1280, 720 );
            mCamera.setParameters(parameters);

            mCameraPreview = new CameraPreview(EyebrowMain.this, mCamera);
            mPreviewFrame = (FrameLayout) findViewById(R.id.fullscreen_content);
            mPreviewFrame.addView(mCameraPreview);
            mCameraPreview.startCameraPreview();
        } else {
            //camera is unavailable
        }
    }

    /**
     * Clear any existing preview / camera.
     */
    private void releaseCameraAndPreview() {

        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }

        //remove the preview from the framelayout
        if(mCameraPreview != null){
            mCameraPreview.destroyDrawingCache();
            mCameraPreview.mCamera = null;
            if (mPreviewFrame != null){
                mPreviewFrame.removeAllViews();
            }
        }

    }

    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            mCameraData = data;

            //stop preview and wait for user to accept or discard the image
            mCamera.stopPreview();
            mCameraButtons.setVisibility(View.GONE);
            mImageButtons.setVisibility(View.VISIBLE);
        }
    };


    private File getOutputMediaFile() {
        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "EyebrowApp");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("EyebrowApp", "failed to create directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }

    private void galleryAddPic(Uri contentUri) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private String getImagePath(Uri uri){

        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }

    private String convertUri(Uri contentUri) {
        String imgUri = "";
        if ("content".equals(contentUri.getScheme())){
            imgUri = getImagePath(contentUri);
        } else {
            imgUri = contentUri.getPath();
        }

        return imgUri;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.shutter_customized_camera:
                if (mCamera != null){
                    mCamera.takePicture(null, null, mPicture);
                    Toast.makeText(EyebrowMain.this, "Picture taken!", Toast.LENGTH_SHORT).show();
                } else {
                    releaseCameraAndPreview();
                    getCameraSetPreview();
                }
                break;
            case R.id.gallery_customized_camera:
                releaseCameraAndPreview();
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT).setType("image/*");
                startActivityForResult(intent, 1);
                break;
            case R.id.accept_customized_camera:
                //Need to move this to background thread;

                savePhotoImage();

                Uri outputUri = Uri.fromFile(new File(getCacheDir(), "cropped"));
                new Crop(mContentUri).output(outputUri).asSquare().start(this);

//                Intent eyeBrowEdit = new Intent(this, EyebrowContainer.class);
//                eyeBrowEdit.setData(mContentUri);
//                startActivity(eyeBrowEdit);

//                Intent eyeBrowEdit = new Intent(this, EyebrowEdit.class);
//                Bundle extras = new Bundle();
//                extras.putString("imageUri", mContentUri.toString());
//                eyeBrowEdit.putExtras(extras);
//                startActivity(eyeBrowEdit);
                break;
            case R.id.discard_customized_camera:
                mImageButtons.setVisibility(View.GONE);
                mCameraButtons.setVisibility(View.VISIBLE);

                releaseCameraAndPreview();
                getCameraSetPreview();
                break;
            case R.id.popup_customized_camera:
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(EyebrowMain.this, mPopupMenu);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.set_eyebrows:
                                if (item.isChecked()){

                                    mPreviewFrame.removeViewAt(1);
                                    item.setChecked(false);

                                } else {
                                    Toast.makeText(EyebrowMain.this, "You Clicked : Find Eyebrows", Toast.LENGTH_SHORT).show();
                                    //releaseCameraAndPreview();
                                    String test = convertUri(mContentUri);
                                    FaceDetectionView myView = new FaceDetectionView(EyebrowMain.this, 0, convertUri(mContentUri));
                                    mPreviewFrame.addView(myView);
                                    item.setChecked(true);
                                }
                                break;
                            case R.id.set_eyebrows_1:
                                if (item.isChecked()){

                                    mPreviewFrame.removeViewAt(1);
                                    item.setChecked(false);

                                } else {
                                    Toast.makeText(EyebrowMain.this, "You Clicked : Find Eyebrows", Toast.LENGTH_SHORT).show();

                                    //releaseCameraAndPreview();
                                    FaceDetectionView myView = new FaceDetectionView(EyebrowMain.this, 1, convertUri(mContentUri));
                                    mPreviewFrame.addView(myView);
                                    item.setChecked(true);
                                }
                                break;
                            case R.id.action_settings:
                                Toast.makeText(EyebrowMain.this, "You Clicked : action settings", Toast.LENGTH_SHORT).show();
                                break;
                        }
                        return true;
                    }
                });

                popup.show();//showing popup menu
                break;
        }
    }

    private void savePhotoImage() {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            return;
        }
        try {
            //update mContentURI to update gallery
            mContentUri = Uri.fromFile(pictureFile);

            FileOutputStream fos = new FileOutputStream(pictureFile);
            fos.write(mCameraData);
            fos.close();

            galleryAddPic(mContentUri);
            //find eyebrows here!
            Toast.makeText(EyebrowMain.this, "Photo Saved!", Toast.LENGTH_LONG).show();
            mCameraButtons.setVisibility(View.VISIBLE);
            mImageButtons.setVisibility(View.GONE);

            //Fix later, figure out image orientation
            getCameraPhotoOrientation(EyebrowMain.this, mContentUri, pictureFile.getAbsolutePath());
            //update the frame layout
            releaseCameraAndPreview();
            displayImage(mContentUri);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}