package shyangk.eyebrowv2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.media.FaceDetector;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by ska38 on 2014-10-01.
 */
public class FaceDetectionView extends View {

    private static final int MAX_FACES = 1;

    private Bitmap background_image;
    private FaceDetector faceDetector;
    private FaceDetector.Face[] face;
    private int numFaces;

    private String mimgUri;

    // preallocate for onDraw(...)
    private PointF tmp_point = new PointF();
    private PointF tmp_point2 = new PointF();

    private Paint tmp_paint = new Paint();
    private int eyebrowId;

    private int offsetH;

    public FaceDetectionView(Context context, int eyebrow_id, String imgUri){
        super(context);
        this.eyebrowId = eyebrow_id;
        this.mimgUri = imgUri;

        this.detectFace();
    }

    private void detectFace() {
        background_image = resizeBitmap(mimgUri);


        faceDetector = new FaceDetector(background_image.getWidth(), background_image.getHeight(), MAX_FACES);

        face = new FaceDetector.Face[MAX_FACES];
        numFaces = faceDetector.findFaces(background_image, face);
    }

    public Bitmap resizeBitmap(String imgUri){

        DisplayMetrics metrics = this.getContext().getResources().getDisplayMetrics();
        int dstWidth = metrics.widthPixels;
        int dstHeight = metrics.heightPixels;

        Bitmap resizedBitmap = null;

        try
        {
            int inWidth = 0;
            int inHeight = 0;

            InputStream in = new FileInputStream(imgUri);

            // decode image size (decode metadata only, not the whole image)
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, options);
            in.close();
            in = null;

            // save width and height
            inWidth = options.outWidth;
            inHeight = options.outHeight;

            // decode full image pre-resized
            in = new FileInputStream(imgUri);
            options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            // calc rought re-size (this is no exact resize)
            options.inSampleSize = Math.max(inWidth/dstWidth, inHeight/dstHeight);

            // decode full image
            Bitmap roughBitmap = BitmapFactory.decodeStream(in, null, options);

            // calc exact destination size
            Matrix m = new Matrix();
            RectF inRect = new RectF(0, 0, roughBitmap.getWidth(), roughBitmap.getHeight());
            RectF outRect = new RectF(0, 0, dstWidth, dstHeight);
            m.setRectToRect(inRect, outRect, Matrix.ScaleToFit.CENTER);
            float[] values = new float[9];
            m.getValues(values);

            // resize bitmap
            resizedBitmap = Bitmap.createScaledBitmap(roughBitmap, (int) (roughBitmap.getWidth() * values[0]), (int) (roughBitmap.getHeight() * values[4]), true);

            offsetH = dstHeight - resizedBitmap.getHeight();


        }
        catch (IOException e)
        {
            Log.e("Image", e.getMessage(), e);
        }
        return resizedBitmap;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //canvas.drawBitmap(background_image, 0, offsetH/2, null);
        if (numFaces > 0) {
            //draw on face
            FaceDetector.Face myface = face[0];
            myface.getMidPoint(tmp_point);
            myface.getMidPoint(tmp_point2);
            tmp_paint.setColor(Color.RED);
            tmp_paint.setAlpha(100);
            myface.getMidPoint(tmp_point);
            tmp_point.offset(0, offsetH/2 - myface.eyesDistance()/5);
            tmp_point2.offset(myface.eyesDistance(), -myface.eyesDistance()/2);

            //TESTING
            Bitmap testeyebrow = null;
            if (eyebrowId == 0){
                testeyebrow = BitmapFactory.decodeResource(this.getResources(), R.drawable.eyebrow);
            } else if (eyebrowId == 1) {
                testeyebrow = BitmapFactory.decodeResource(this.getResources(), R.drawable.eyebrow1);
            }

            Matrix m = new Matrix();
            RectF inRect = new RectF(0, 0, testeyebrow.getWidth(), testeyebrow.getHeight());
            RectF outRect = new RectF(0, 0, myface.eyesDistance() * 2, myface.eyesDistance() * 2);
            m.setRectToRect(inRect, outRect, Matrix.ScaleToFit.CENTER);

            float[] values = new float[9];
            m.getValues(values);

            // resize bitmap
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(testeyebrow, (int) (testeyebrow.getWidth() * values[0]), (int) (testeyebrow.getHeight() * values[4]), true);

            canvas.drawBitmap(resizedBitmap, tmp_point.x - resizedBitmap.getWidth()/2, tmp_point.y - resizedBitmap.getHeight()/2, null);
            //canvas.drawLine(tmp_point.x, tmp_point.y, tmp_point2.x, tmp_point2.y, tmp_paint );
            canvas.drawCircle(tmp_point.x, tmp_point.y, myface.eyesDistance(),tmp_paint);

        } else {
            Toast.makeText(this.getContext(), "No faces found", Toast.LENGTH_LONG).show();
        }
    }
}
