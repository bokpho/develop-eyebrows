package shyangk.eyebrowv2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by ska38 on 2014-10-01.
 */
public class PaintDropperView extends View {

    private Bitmap background_image, eyeDropper;
    private int mColor;

    private Paint drawPaint;

    float touchX;
    float touchY;


    private Button mColorButton;

    public PaintDropperView(Context context, FrameLayout frameLayout, Button button){
        super(context);

        updateDrawingCache(frameLayout);
        this.mColorButton = button;

        touchX = background_image.getWidth()/2;
        touchY = background_image.getHeight()/2;

        //Initialize variables
        drawPaint = new Paint();
        drawPaint.setColor(0xFF660000);

        eyeDropper = BitmapFactory.decodeResource(getResources(), R.drawable.ic_eyedropper_white);


    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(eyeDropper, touchX, touchY, drawPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
//detect user touch
        touchX = event.getX();
        touchY = event.getY();

        int pixel = background_image.getPixel((int) touchX, (int) touchY);
        mColor = Color.rgb(Color.red(pixel), Color.green(pixel), Color.blue(pixel));

        mColorButton.setBackgroundColor(mColor);

        Log.i("pixel", "at : " + pixel);
        Log.i("color", "color: " + mColor);

        invalidate();
        return true;

    }

    public void updateDrawingCache(FrameLayout frameLayout){
        frameLayout.buildDrawingCache();
        Bitmap b1 = frameLayout.getDrawingCache();
        // copy this bitmap otherwise distroying the cache will destroy
        // the bitmap for the referencing drawable and you'll not
        // get the captured view
        background_image = b1.copy(Bitmap.Config.ARGB_8888, false);

        frameLayout.destroyDrawingCache();
    }

    public int getColor(){
        return mColor;
    }
}
