package shyangk.eyebrowv2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.media.FaceDetector;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Stack;

/**
 * Created by ska38 on 2014-10-01.
 */
public class RemoveEyebrowView extends View {


    private int mColor, mPaintBrushSize;

    //drawing path
    private Path drawPath;
    //drawing and canvas paint
    private Paint drawPaint, canvasPaint, testPaint;
    //initial color
    private int paintColor = 0xFF660000;
    //canvas
    private Canvas drawCanvas;
    //canvas bitmap
    private Bitmap canvasBitmap;
    private boolean eraserMode;


    private LinkedList<Path> mUndoDrawPaths, mRedoDrawPaths;
    private int mPosition;

    public RemoveEyebrowView(Context context, int paintBrush, int color){
        super(context);
        this.mColor = color;
        this.mPaintBrushSize = paintBrush;
        this.eraserMode = false;

        setupDrawing();
    }

    private void setupDrawing() {
        //initialize drawPath
        mUndoDrawPaths = new LinkedList<Path>();
        mRedoDrawPaths = new LinkedList<Path>();

        //get drawing area setup for interaction
        drawPath = new Path();
        drawPaint = new Paint();
        drawPaint.setColor(mColor);
        drawPaint.setAlpha(45);

        drawPaint.setMaskFilter(new BlurMaskFilter(10, BlurMaskFilter.Blur.NORMAL));

        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(mPaintBrushSize);

        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);

        canvasPaint = new Paint(Paint.DITHER_FLAG);

    }

    //TODO: Need to keep track of paint strokes for undo/redo
    public void setPaintStrokeWidth(int paintBrushSize){
        drawPaint.setStrokeWidth(paintBrushSize);
    }

    public void setPaintColor(int color){
        drawPaint.setColor(color);
    }

    public void redoLastPath(){
        if (mRedoDrawPaths.peek() != null){
            mUndoDrawPaths.push(mRedoDrawPaths.pop());
            //Redraw the canvas
            drawCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
            for (Path p : mUndoDrawPaths) {
                drawCanvas.drawPath(p, drawPaint);
            }
            invalidate();
        }
    }

    public void eraseLastPath(){
        if (mUndoDrawPaths.peek() != null) {
            mRedoDrawPaths.push(mUndoDrawPaths.pop());
            //Redraw the canvas
            drawCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
            for (Path p : mUndoDrawPaths) {
                drawCanvas.drawPath(p, drawPaint);
            }
            invalidate();
        }
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
    //view given size
        super.onSizeChanged(w, h, oldw, oldh);
        canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        drawCanvas = new Canvas(canvasBitmap);

    }

    @Override
    protected void onDraw(Canvas canvas) {
    //draw view

        canvas.drawPath(drawPath, drawPaint);
        canvas.drawBitmap(canvasBitmap, 0, 0, canvasPaint);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
//detect user touch
        float touchX = event.getX();
        float touchY = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                drawPath.moveTo(touchX, touchY);
                break;
            case MotionEvent.ACTION_MOVE:
                drawPath.lineTo(touchX, touchY);
                break;
            case MotionEvent.ACTION_UP:
                drawCanvas.drawPath(drawPath, drawPaint);

                Path temp = new Path();
                temp.set(drawPath);
                mUndoDrawPaths.push(temp);

                drawPath.reset();
                break;
            default:
                return false;
        }

        invalidate();
        return true;

    }

}
