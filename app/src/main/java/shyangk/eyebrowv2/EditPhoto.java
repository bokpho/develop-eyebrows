package shyangk.eyebrowv2;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.media.FaceDetector;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;


public class EditPhoto extends Fragment implements View.OnClickListener {
    public static final String TAG = EditPhoto.class.getSimpleName();

    private OnFragmentInteractionListener mListener;
    private String mContentUri;
    private String mInternalUri;

    private FrameLayout mProgressBar;
    private ImageButton eyeDropper;
    private ImageButton paintBrush;
    private ImageButton undoButton;
    private ImageButton redoButton;
    private Button colorButton;

    //Paint Brush Sizes
    private ImageButton smallBrush;
    private ImageButton mediumBrush;
    private ImageButton largeBrush;
    private ImageButton xlargeBrush;

    //Brush Size int Values
    public static final int smallSize = 10;
    public static final int mediumSize = 15;
    public static final int largeSize = 20;
    public static final int xlargeSize = 25;

    private LinearLayout paintBrushSize;
    private int mSelectedPaintBrush = smallSize;
    private int mPaintColor = 0xFF660000;

    RemoveEyebrowView mRemoveBrow;
    PaintDropperView mPaintDropperView;

    private PointF tmp_point = new PointF();

    private Bitmap mPhoto;
    private FrameLayout mFrameLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = this.getArguments();
        mContentUri = bundle.getString("Uri");
        mInternalUri = bundle.getString("InternalURI");

        detectFace detectFc = new detectFace();
        detectFc.execute(mContentUri);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_photo, container, false);
        TextView textView = (TextView) getActivity().findViewById(R.id.action_name);
        textView.setText("EDIT PHOTO");

        mFrameLayout = (FrameLayout) getActivity().findViewById(R.id.fullscreen_content);

        mProgressBar = (FrameLayout) getActivity().findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.VISIBLE);


        eyeDropper = (ImageButton) view.findViewById(R.id.edit_eyedropper);
        paintBrush = (ImageButton) view.findViewById(R.id.edit_paintbrush);
        undoButton = (ImageButton) view.findViewById(R.id.edit_undo);
        redoButton = (ImageButton) view.findViewById(R.id.edit_redo);

        colorButton = (Button) view.findViewById(R.id.edit_paint_color);

        //Get the views for the brushes
        smallBrush = (ImageButton) view.findViewById(R.id.small_brush);
        mediumBrush = (ImageButton) view.findViewById(R.id.medium_brush);
        largeBrush = (ImageButton) view.findViewById(R.id.large_brush);
        xlargeBrush = (ImageButton) view.findViewById(R.id.xlarge_brush);

        //Default brush is the small brush
        smallBrush.setSelected(true);

        paintBrushSize = (LinearLayout) view.findViewById(R.id.edit_brushholder);

        return view;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        eyeDropper.setOnClickListener(this);
        paintBrush.setOnClickListener(this);
        undoButton.setOnClickListener(this);
        redoButton.setOnClickListener(this);

        //Add listeners for the brushes
        smallBrush.setOnClickListener(this);
        mediumBrush.setOnClickListener(this);
        largeBrush.setOnClickListener(this);
        xlargeBrush.setOnClickListener(this);

        mRemoveBrow = new RemoveEyebrowView(this.getActivity(), mSelectedPaintBrush, mPaintColor);

    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.edit_eyedropper:
                paintBrushSize.setVisibility(View.GONE);

                if (eyeDropper.isSelected()){
                    eyeDropper.setSelected(false);

                    //Remove last view
                    removeView(mPaintDropperView);
                } else {
                    paintBrush.setSelected(false);
                    eyeDropper.setSelected(true);

                    if (mPaintDropperView == null) {
                        mPaintDropperView = new PaintDropperView(this.getActivity(), mFrameLayout, colorButton);
                    } else {
                        mPaintDropperView.updateDrawingCache(mFrameLayout);
                    }
                    addView(mPaintDropperView);

                }
                break;
            case R.id.edit_paintbrush:
                if (paintBrush.isSelected()){
                    paintBrush.setSelected(false);

                    paintBrushSize.setVisibility(View.GONE);
                } else {
                    paintBrush.setSelected(true);
                    eyeDropper.setSelected(false);

                    paintBrushSize.setVisibility(View.VISIBLE);

                    if (mPaintDropperView != null){
                        mPaintColor = mPaintDropperView.getColor();
                        //Remove if mPaintDropperView is on top
                        removeView(mPaintDropperView);
                    }

                    //If mRemovebRow is not added to mFrameLayout then add it
                    if (!mListener.drawViewSet(mRemoveBrow)){
                        addView(mRemoveBrow);
                    }
                    //Set the paint selected
                    mRemoveBrow.setPaintColor(mPaintColor);
                }
                break;
            case R.id.edit_undo:
                mRemoveBrow.eraseLastPath();
                break;
            case R.id.edit_redo:
                mRemoveBrow.redoLastPath();
                break;
            case R.id.small_brush:
                smallBrush.setSelected(true);
                mediumBrush.setSelected(false);
                largeBrush.setSelected(false);
                xlargeBrush.setSelected(false);

                mSelectedPaintBrush = smallSize;
                mRemoveBrow.setPaintStrokeWidth(mSelectedPaintBrush);
                break;
            case R.id.medium_brush:
                smallBrush.setSelected(false);
                mediumBrush.setSelected(true);
                largeBrush.setSelected(false);
                xlargeBrush.setSelected(false);

                mSelectedPaintBrush = mediumSize;
                mRemoveBrow.setPaintStrokeWidth(mSelectedPaintBrush);
                break;
            case R.id.large_brush:
                smallBrush.setSelected(false);
                mediumBrush.setSelected(false);
                largeBrush.setSelected(true);
                xlargeBrush.setSelected(false);

                mSelectedPaintBrush = largeSize;
                mRemoveBrow.setPaintStrokeWidth(mSelectedPaintBrush);
                break;
            case R.id.xlarge_brush:
                smallBrush.setSelected(false);
                mediumBrush.setSelected(false);
                largeBrush.setSelected(false);
                xlargeBrush.setSelected(true);

                mSelectedPaintBrush = xlargeSize;
                mRemoveBrow.setPaintStrokeWidth(mSelectedPaintBrush);
                break;
        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) activity;
        } else {
            throw new ClassCastException(activity.toString()
                    + " must implemenet MyListFragment.OnItemSelectedListener");
        }
    }

    public void editImage(int paintBrushSize, int color){
        //Start the RemoveEyebrowView using the selected paintBrushSize and color
        if (mRemoveBrow == null) {
            mRemoveBrow = new RemoveEyebrowView(this.getActivity(), paintBrushSize, color);
            mListener.addToPreviewFrame(mRemoveBrow);
        } else {
            //Get new color
            mRemoveBrow.setPaintColor(mPaintColor);
            //Compress image
        }
    }


    public void addView(View view){
        mListener.addToPreviewFrame(view);
    }

    public void removeView(View view){
        mListener.removeFromPreviewFrame(view);
    }

    public void removeLastView(){
        mListener.resetCanvas();

    }

    private class detectFace extends AsyncTask <String, Void, Integer> {

        @Override
        protected Integer doInBackground(String[] strings) {
            int count = strings.length;
            int face_count;
            int color = 0;
            Log.i(TAG, "Found uris: " + count + " at " + strings[0]);
            BitmapFactory.Options bitmap_options = new BitmapFactory.Options();

            bitmap_options.inPreferredConfig = Bitmap.Config.RGB_565;
            for (int i = 0; i < count; i++) {
                Bitmap background_image = BitmapFactory.decodeFile(strings[i], bitmap_options);

                if(((background_image.getWidth()%2)) != 0){
                    background_image = Bitmap.createScaledBitmap(background_image,
                            background_image.getWidth()+1, background_image.getHeight(), false);
                }

                FaceDetector face_detector = new FaceDetector(
                        background_image.getWidth(), background_image.getHeight(),
                        1);



                FaceDetector.Face[] faces = new FaceDetector.Face[1];

                face_count = face_detector.findFaces(background_image, faces);

                if (face_count > 0) {


                    faces[0].getMidPoint(tmp_point);

                    int mid = background_image.getPixel(Math.round(tmp_point.x), Math.round(tmp_point.y));

                    int red = Color.red(mid);
                    int green = Color.green(mid);
                    int blue = Color.blue(mid);

                    color = Color.rgb(red, green, blue);
                }

            }

            return color;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            setBrushColor(integer.intValue());
        }
    }

    private void setBrushColor(int color) {
        mProgressBar.setVisibility(View.INVISIBLE);

        colorButton.setBackgroundColor(color);
        mPaintColor = color;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void addToPreviewFrame(View view);
        public void removeFromPreviewFrame(View view);
        public void resetCanvas();
        public boolean drawViewSet(View view);

    }

}
